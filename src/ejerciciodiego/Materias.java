/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciodiego;

import java.util.Scanner;

/**
 *
 * @author USUARIO
 */
public class Materias {
    
    private Scanner teclado;
    private char[][] estudiantes;
    private int[][] materias;
    private int[] promedios; 
    
    /**
     * @return the teclado
     */
    public Scanner getTeclado() {
        return teclado;
    }

    /**
     * @param teclado the teclado to set
     */
    public void setTeclado(Scanner teclado) {
        this.teclado = teclado;
    }

    /**
     * @return the estudiantes
     */
    public char[][] getEstudiantes() {
        return estudiantes;
    }

    /**
     * @param estudiantes the estudiantes to set
     */
    public void setEstudiantes(char[][] estudiantes) {
        this.estudiantes = estudiantes;
    }

    /**
     * @return the materias
     */
    public int[][] getMaterias() {
        return materias;
    }

    /**
     * @param materias the materias to set
     */
    public void setMaterias(int[][] materias) {
        this.materias = materias;
    }

    /**
     * @return the promedios
     */
    public int[] getPromedios() {
        return promedios;
    }

    /**
     * @param promedios the promedios to set
     */
    public void setPromedios(int[] promedios) {
        this.promedios = promedios;
    }
    
}
    

